package tn.esprit.demo;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CdController {

    /**
     * Handle the root (/) endpoint and return the start page.
     * @return
     */

    @RequestMapping("/")
    public String index(){
        return "start";
    }
}
