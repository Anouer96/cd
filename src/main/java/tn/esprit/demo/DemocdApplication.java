package tn.esprit.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemocdApplication {

    public static void main(String[] args) {
        SpringApplication.run(DemocdApplication.class, args);
    }

}
